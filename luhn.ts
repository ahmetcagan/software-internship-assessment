import { listeners } from "cluster";

const stdin = process.openStdin();


stdin.addListener("data", (data) => {
    const isValid = luhnCheck(data.toString())
    process.exit(isValid ? 0 : 42);

});

function luhnCheck(inputValue: string): boolean {
    
    //Removing all empty spaces
    let tempString = inputValue.trim().replace(/\s+/g,'');
    
    //Input can not be only one digit
    if (tempString.length == 1){
        return false
    }

    //Does input contain only digits?
    let regexNumber = /^\+?\d+$/
    
    if(!regexNumber.test(tempString)){
        return false
    }
    
    //stringArray to numberArray
    const numberArray = tempString.split('').map((item)=>{
        return parseInt(item)
    });


    let sum = numberArray[numberArray.length-1]
    const nDigits = numberArray.length
    const parity = nDigits%2;

    for(let i = nDigits - 2; i >=0 ; i-=1){
        let digit = numberArray[i]
        
        if(i%2 === parity){
           digit = digit*2;
        }
        if (digit>9)
            digit = digit-9;

        sum += digit;
    } 

    return sum%10 === 0;
}